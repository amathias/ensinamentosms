import React from 'react';
import { StackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';

import reducers from './src/reducers'

import Main from './src/components/Main';
import CardContent from './src/components/CardContent';
import SearchScreen from './src/components/SearchScreen';
import Bookshelf from './src/components/Bookshelf';

const RootStack = StackNavigator(
  {
    Home: Main,
    Bookshelf: Bookshelf,
    Content: CardContent,
    Search: SearchScreen,
  },
  {
    initialRouteName: 'Home',
    /* The header config from HomeScreen is now here */
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#699EB0',
      },
      headerTintColor: '#FFF',
      headerTitleStyle: {
        fontWeight: 'bold',
      }
    },
  }

);

const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
const persistor = persistStore(store)

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    
    await Expo.Font.loadAsync({
      'Helvetica-Condensed': require('./src/fonts/helvetica_condensed.ttf'),
      'Helvetica-Condensed-Bold': require('./src/fonts/helvetica_condensed_bold.ttf'),
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    });

    this.setState({ loading: false });
  }

  render() {

    if (this.state.loading) {
      return <Expo.AppLoading />;
    }
    
    return(
      <Provider store={store} >
        <PersistGate loading={null} persistor={persistor}>
          <RootStack />
        </PersistGate>
      </Provider>
    );
  }

}
