package mathias.japp.ensinamentomswebparser;

import java.util.List;

public class Book {
    
    private String title;
    
    private List<Teaching> teachings;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Teaching> getTeachings() {
        return teachings;
    }

    public void setTeachings(List<Teaching> teachings) {
        this.teachings = teachings;
    }

    @Override
    public String toString() {
	return "Book [title=" + title + ", teachings=" + teachings + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((teachings == null) ? 0 : teachings.hashCode());
	result = prime * result + ((title == null) ? 0 : title.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Book other = (Book) obj;
	if (teachings == null) {
	    if (other.teachings != null)
		return false;
	} else if (!teachings.equals(other.teachings))
	    return false;
	if (title == null) {
	    if (other.title != null)
		return false;
	} else if (!title.equals(other.title))
	    return false;
	return true;
    }
    
    
}
