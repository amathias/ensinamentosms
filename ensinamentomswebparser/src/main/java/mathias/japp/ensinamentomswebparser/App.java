package mathias.japp.ensinamentomswebparser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.text.WordUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.google.gson.Gson;

/**
 * Hello world!
 *
 */
public class App {
    
    private static final String URL = "http://novaeraalicercedoparaiso.blogspot.com.br/2010/05/alicerce-do-paraiso-vol-04.html";
    
    private static int idxBegin = -1;
    
    private static Map<String, Book> bookshelf = new HashMap<>();
    
    private static List<Teaching> teachingsList = new ArrayList<>();
    
    public static void main(String[] args) throws Exception {
	
	final Document doc = Jsoup.connect(URL).get();
	
	System.out.println(doc.charset());

	final String title = extractTitle( doc );
	final String post = extractPost( doc );
	
	findIdxBegin( post );
	
//	final String summary = extractSummary( post );
	final String teachings = extractTeachings( post );

	final Book book = new Book();
	book.setTitle( WordUtils.capitalize(title) );
	book.setTeachings( teachingsList );
	
	final String [] array = extract(teachings);

//	System.out.println(Arrays.toString(array));
	
	for( String _arr : array  ) {
//	    System.out.println( _arr );
	    parse( _arr );
	}
	
	final String key = String.join("-", title.toLowerCase().split(" "));
	bookshelf.put(key, book);
	
//	System.out.println( book );
	
	final Gson gson = new Gson();
	
	System.out.println( gson.toJson(bookshelf) );
    }
    
    private static String extractTitle( final Document doc ) {

	final Element element = doc.selectFirst(".post-title");
	
	return element.html();
    }
    
    private static String extractPost( final Document doc ) {
	
	final Element element = doc.selectFirst(".post-body");
	final String html = element.html().replaceAll("\\.{2,}[0-9]+\\n*", "\n");  
	
	return html;
    }
    
    private static void findIdxBegin( final String post ) {
//	System.out.println(post);
	int idxEndSummary = post.lastIndexOf("<br>\n<br>\n<br>\n<br>");
	String extract = post.substring(0, idxEndSummary);
	System.out.println(extract);
	idxEndSummary = extract.lastIndexOf("<br>\n<br>");
	extract = post.substring(0, idxEndSummary);

	idxBegin = extract.lastIndexOf("<br>\n<br>");
	if ( idxBegin < 0) idxBegin = extract.length();
	
    }
    
    private static String extractSummary( final String post ) {
//	System.out.println(post);
	final String extract = post.substring(0, idxBegin);
	
	return extract;
    }
    
    private static String extractTeachings( final String post ) {
	
	final String partial = post.substring( idxBegin );
	
	final int idxBr = partial.indexOf( "<br>" );
	final int idxDiv = partial.indexOf("<div"); 
	
	final String teachings = partial.substring( idxBr, idxDiv );
	
	return teachings;
    }
    
    private static String[] extract(final String teachings ) {
//	System.out.println(teachings);
	final String[] thchns = teachings.split("(\n<br>\n<br>[[:alpha:]]*\n)");
//	System.out.println( Arrays.toString(thchns) );
	return thchns;
    }
    
    private static void parse( String part ) {
	
	
	final String[] split = part.split("<br>\n<br>");
	
	final String subpart = split[split.length-1];
	final int idx = subpart.lastIndexOf("\n<br>");
	
	final String title = clean( split[split.length-2] ).replaceAll("\n", "");
	final String content = clean( subpart.substring(0, idx) );
	final String info = clean( subpart.substring(idx) ).replaceAll("\n", "");
	
//	System.out.println("TITLE: " + title);
//	System.out.println(content);
//	System.out.println(info);
	
	if ( title.isEmpty() || content.isEmpty() || info.isEmpty() ) {
	    System.out.println(part);
	}

	private static String extractTeachings(final String post) {

		final String partial = post.substring(idxBegin);

		final int idxBr = partial.indexOf("<br>");
		final int idxDiv = partial.indexOf("<div");

		final String teachings = partial.substring(idxBr, idxDiv);

		return teachings;
	}

	private static String[] extract(final String teachings) {
		// System.out.println(teachings);
		final String[] thchns = teachings.split("(\n<br>\n<br>[[:alpha:]]*\n)");
		// System.out.println( Arrays.toString(thchns) );
		return thchns;
	}

	private static void parse(String part) {

		final String[] split = part.split("<br>\n<br>");

		final String subpart = split[split.length - 1];
		final int idx = subpart.lastIndexOf("\n<br>");

		final String title = clean(split[split.length - 2]).replaceAll("\n", "");
		final String content = clean(subpart.substring(0, idx));
		final String info = clean(subpart.substring(idx)).replaceAll("\n", "");

		// System.out.println("TITLE: " + title);
		// System.out.println(content);
		// System.out.println(info);

		if (title.isEmpty() || content.isEmpty() || info.isEmpty()) {
			System.out.println(part);
		}

		final Teaching t = new Teaching();
		t.setTitle(WordUtils.capitalize(title.toLowerCase()));
		t.setContent(content);
		t.setInfo(info);
		t.setAuthor("Meishu-Sama");

		teachingsList.add(t);
	}

	private static String clean(final String str) {

		return str.replace("<br>\n<br>", "").replaceAll("<br>", "");
	}

}
