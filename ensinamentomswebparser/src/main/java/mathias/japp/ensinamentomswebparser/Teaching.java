package mathias.japp.ensinamentomswebparser;

public class Teaching {
    
    private String title;
    
    private String subtitle;
    
    private String content;
    
    private String info;
    
    private String author;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
	return "Teaching [title=" + title + ", subtitle=" + subtitle + ", content=" + content + ", info=" + info
		+ ", author=" + author + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((author == null) ? 0 : author.hashCode());
	result = prime * result + ((content == null) ? 0 : content.hashCode());
	result = prime * result + ((info == null) ? 0 : info.hashCode());
	result = prime * result + ((subtitle == null) ? 0 : subtitle.hashCode());
	result = prime * result + ((title == null) ? 0 : title.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Teaching other = (Teaching) obj;
	if (author == null) {
	    if (other.author != null)
		return false;
	} else if (!author.equals(other.author))
	    return false;
	if (content == null) {
	    if (other.content != null)
		return false;
	} else if (!content.equals(other.content))
	    return false;
	if (info == null) {
	    if (other.info != null)
		return false;
	} else if (!info.equals(other.info))
	    return false;
	if (subtitle == null) {
	    if (other.subtitle != null)
		return false;
	} else if (!subtitle.equals(other.subtitle))
	    return false;
	if (title == null) {
	    if (other.title != null)
		return false;
	} else if (!title.equals(other.title))
	    return false;
	return true;
    }
    
}
