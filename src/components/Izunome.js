
import React, { Component } from 'react';
import { View } from 'react-native';
import Image from 'react-native-remote-svg';

const Izunome = () => {
    return(
      <Image source={{uri: 'http://www.messianica.org.br/images/igreja-messianica-reduzido.svg' }}
        style={{
          paddingVertical: 5
        }}
      />
    );
  };

  export default Izunome;