import React, { Component } from 'react';
import {View, Text, Image, TouchableHighlight, ActivityIndicator} from 'react-native';
import {
  Container,
  Content,
  H1,
  Spinner,
  Footer
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { connect } from 'react-redux';

import { loadMessianicaMainContent, loadMessianicaNewsContent } from '../actions';

import books from '../resources/books.json';
import Agenda from './Agenda';
import About from "./About";
import Izunome from './Izunome';

const ENSINAMENTO_IMAGE_URL = 'http://revistaizunome.messianica.org.br/cms/categorias/4.jpg';
const EXPERIENCIA_IMAGE_URL = 'http://revistaizunome.messianica.org.br/cms/categorias/5.jpg';
const PALESTRA_IMAGE_URL = 'http://revistaizunome.messianica.org.br/cms/categorias/2.jpg';

class Main extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    return {
      headerTitle: <Izunome />
    }
  };

  goToBookshelf() {
    this.props.navigation.navigate('Bookshelf', { content : books } );
  }

  goToEnsinamentoDia() {

      if ( !this.props.content ) {
          return;
      }
      
      const { ensinamento } = this.props.content;
      this.props.navigation.navigate('Content', { content : ensinamento.dia } );
  }

  goToEnsinamentoMes() {

    if ( !this.props.content ) {
        return;
    }
    
    const { ensinamento } = this.props.content;
    this.props.navigation.navigate('Content', { content : ensinamento.mes } );
}

  goToExperienciaFeDia() {

      if ( !this.props.content ) {
          return;
      }
      
      const { experiencia } = this.props.content;
      this.props.navigation.navigate('Content', { content : experiencia.dia } );
  }

  goToExperienciaFeMes() {

    if ( !this.props.content ) {
        return;
    }
    
    const { experiencia } = this.props.content;
    this.props.navigation.navigate('Content', { content : experiencia.mes } );
  }

  goToPalestraMes() {

    if ( !this.props.content ) {
        return;
    }
    
    const { palestra } = this.props.content;
    this.props.navigation.navigate('Content', { content : palestra.mes } );
  }

  componentDidMount() {
    this.props.loadMessianicaNewsContent();
    this.props.loadMessianicaMainContent();
  }

  renderImagemEnsinamento() {
    
    return <Image source={{uri: ENSINAMENTO_IMAGE_URL }}
      style={{
        flex: 1,
        alignSelf: 'stretch',
      }} />

  }

  renderEnsinamentoDia() {

    return(
      <View style={{ flex: 1 }}>
        <TouchableHighlight
                  style={
                    [ styles.centerStyle, { borderColor: '#699EB0', borderBottomWidth: 1 }
                    ] }
                  underlayColor={'#699EB0'}
                  onPress={ this.goToEnsinamentoMes.bind(this) } >
            { ( this.props.content && this.props.content.ensinamento && this.props.content.ensinamento.mes ?
                <View style={[ styles.centerStyle, { flex: 1,  alignSelf: 'stretch'} ]} >
                    <Text style={{fontSize: 18, fontFamily: 'Helvetica-Condensed', color: '#FFF'}}>
                      Ensinamento do mês
                    </Text>
                </View>
                : <ActivityIndicator color="white" size='small' /> )
              }

          </TouchableHighlight>
          <TouchableHighlight
                  style={
                    [ styles.centerStyle
                    ] }
                  underlayColor={'#699EB0'}
                  onPress={ this.goToEnsinamentoDia.bind(this) } >
            { ( this.props.content && this.props.content.ensinamento && this.props.content.ensinamento.dia ?
                <View style={[ styles.centerStyle, { flex: 1,  alignSelf: 'stretch'} ]} >
                    <Text style={{fontSize: 18, fontFamily: 'Helvetica-Condensed', color: '#FFF'}}>
                      Ensinamento do dia
                    </Text>
                </View>
                : <ActivityIndicator color="white" size='small' /> )
              }
          </TouchableHighlight>
          <TouchableHighlight
                  style={
                    [ styles.centerStyle, { borderColor: '#699EB0', borderTopWidth: 1 }
                    ] }
                  underlayColor={'#699EB0'}
                  onPress={ this.goToBookshelf.bind(this) } >
            <Text style={{fontSize: 18, fontFamily: 'Helvetica-Condensed', color: '#FFF'}}>Livros de ensinamentos</Text>
          </TouchableHighlight>
      </View>
    );

  }
  
  renderImagemExperienciaFe() {
    return <Image source={{uri: EXPERIENCIA_IMAGE_URL }}
      style={{
        flex: 1,
        alignSelf: 'stretch',
      }} />
  }

  renderExperienciaFe() {

    return(
      <View style={{ flex: 1 }}>
        <TouchableHighlight
                  style={
                    [ styles.centerStyle, { borderColor: '#699EB0', borderBottomWidth: 1 }
                    ] }
                  underlayColor={'#699EB0'}
                  onPress={ this.goToExperienciaFeMes.bind(this) } >
             { ( this.props.content && this.props.content.experiencia && this.props.content.experiencia.mes ?
                <View style={[ styles.centerStyle, { flex: 1,  alignSelf: 'stretch'} ]} >
                  <Text style={{fontSize: 18, fontFamily: 'Helvetica-Condensed', color: '#FFF'}}>
                    Experiência de fé do mês
                  </Text>
                </View>
                : <ActivityIndicator color="white" size='small' /> )
              }
          </TouchableHighlight>
          <TouchableHighlight
                  style={
                    [ styles.centerStyle
                    ] }
                  underlayColor={'#699EB0'}
                  onPress={ this.goToExperienciaFeDia.bind(this) } >
             { ( this.props.content && this.props.content.experiencia && this.props.content.experiencia.dia ?
                <View style={[ styles.centerStyle, { flex: 1,  alignSelf: 'stretch'} ]} >
                  <Text style={{fontSize: 18, fontFamily: 'Helvetica-Condensed', color: '#FFF'}}>
                  Experiência de fé do dia
                  </Text>
                </View>
                : <ActivityIndicator color="white" size='small' /> )
              }
          </TouchableHighlight>
      </View>
    );

  }
  
  renderImagemPalestras() {
    return <Image source={{uri: PALESTRA_IMAGE_URL }}
      style={{
        flex: 1,
        alignSelf: 'stretch',
      }} />
  }

  renderPalestras() {

    return(
      <View style={{ flex: 1 }}>
          <TouchableHighlight
                  style={
                    [ styles.centerStyle
                    ] }
                  underlayColor={'#699EB0'}
                  onPress={ this.goToPalestraMes.bind(this) } >
             { ( this.props.content && this.props.content.palestra && this.props.content.palestra.mes ?
                <View style={[ styles.centerStyle, { flex: 1,  alignSelf: 'stretch'} ]} >
                  <Text style={{fontSize: 18, fontFamily: 'Helvetica-Condensed', color: '#FFF'}}>
                  Palestra do mês
                  </Text>
                </View>
                : <ActivityIndicator color="white" size='small' /> )
              }
          </TouchableHighlight>
      </View>
    );

  }
  
  render() {

    return (
      <Container>
        <Content style={{ flex: 1 }} >

          <Grid>

            <Row size={1} style={{backgroundColor: '#699EB0'}} >
                <Agenda style={
                    [ styles.centerStyle,
                        { flex: 1, height: 170 }
                    ]} />
            </Row>

            <Row size={1} style={{backgroundColor: '#63BFA5', borderColor: '#699EB0', borderTopWidth: 1}} >
                <Col>
                  <View
                      style={
                        [ styles.centerStyle,
                          { height: 200 }
                        ] }
                      activeOpacity={0.5}>
                    { this.renderImagemEnsinamento() }
                  </View>
                </Col>
                <Col>
                    { this.renderEnsinamentoDia() }
                </Col>
              </Row>
              <Row size={1} style={{backgroundColor: '#63BFA5', borderColor: '#699EB0', borderTopWidth: 1}} >
                <Col>
                    { this.renderExperienciaFe() }
                </Col>
                <Col>
                  <View
                      style={
                        [ styles.centerStyle,
                          { height: 200 }
                        ] }
                      activeOpacity={0.5}>
                    { this.renderImagemExperienciaFe() }
                  </View>
                </Col>
            </Row>
            <Row size={1} style={{backgroundColor: '#63BFA5', borderColor: '#699EB0', borderTopWidth: 1, borderBottomWidth: 1}} >
                <Col>
                  <View
                      style={
                        [ styles.centerStyle,
                          { height: 200 }
                        ] }
                      activeOpacity={0.5}>
                    { this.renderImagemPalestras() }
                  </View>
                </Col>
                <Col>
                    { this.renderPalestras() }
                </Col>
            </Row>
        </Grid>

        </Content>

        {/* <Footer>
          <About/>
        </Footer> */}
        
      </Container>
    );
  }

}

const styles = {

  centerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  }

};

const mapStateToProps = (state) => {
  return { content: state.messianica };
}

export default connect(mapStateToProps, { loadMessianicaMainContent, loadMessianicaNewsContent } )(Main);