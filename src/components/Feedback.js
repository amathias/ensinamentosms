import React, {Component} from 'react';
import {
    Body,
    Button,
    Content,
    Form,
    H3,
    Header,
    Icon,
    Label,
    Left,
    Picker,
    Right,
    Spinner,
    Text,
    Textarea,
    Title,
    View
} from "native-base";
import {Modal, Alert, Platform, StyleSheet} from "react-native";
import {connect} from 'react-redux';
import {enviarFeedback} from "../actions";


const INITIAL_STATE =  {
    isLoading: false,
    title: "Issue",
    content: {
        raw: undefined
    },
    kind: "bug",
    priority: "trivial"
};

class Feedback extends Component {

    constructor(props) {
        super(props);
        this.state = INITIAL_STATE;
    }

    onChangeFeedbackKind(kind) {
        this.setState({
            kind
        });
    }

    onChangeFeedback( feedback ) {
        this.setState({
            content : {
                raw: feedback
            }
        });
    }

    sendFeedback() {

        if ( this.state.isLoading ) {
            return;
        }

        const { title, kind, content } = this.state;

        if ( !content.raw || content .raw === '') {
            Alert.alert(
                'Feedback',
                'Por favor, antes de enviar, escreva o feedback.'
            );
            return;
        }

        this.props.enviarFeedback({
            title,
            kind,
            content
        });

        this.setState({ isLoading: true });
    }

    componentWillUpdate() {
        if (  this.props.feedbackVisible && this.props.content && this.props.content.created) {
            this.props.enviarFeedback( undefined );
            this.setState({...INITIAL_STATE});
        }
    }

    render() {

        if ( this.props.feedbackVisible && this.props.content && this.props.content.created ) {

            Alert.alert(
                'Feedback',
                'Feedback enviado!',
                [
                    {text: 'OK', onPress: () => {
                            this.props.closeFeedback();
                        }},
                ],
                { cancelable: false }
            );
        }

        console.log(this.manufacturer);

        const pickerStyle = StyleSheet.create({
            view: {
                paddingBottom: 20,
                flex: 1,

            },
        });

        return(

            <Modal
                animationType="slide"
                visible={this.props.feedbackVisible}
                onRequestClose={ () => this.props.closeFeedback() }>

                <View style={[styles.centerStyle, { backgroundColor:'#FFFFFF' } ]}>
                    <Header style={{backgroundColor: '#0d5900'}}>
                        <Left>
                            <Button transparent
                                    onPress={ () => this.props.closeFeedback() }>
                                <Icon name='arrow-back' style={{color: '#FFFFFF'}} />
                            </Button>
                        </Left>
                        <Body>
                        <Title style={ {fontFamily: 'Helvetica-Condensed-Bold', color: '#FFFFFF'} }>Feedback</Title>
                        </Body>
                        <Right />
                    </Header>
                    <Content style={{padding: 15}}>
                        <H3  style={ {fontFamily: 'Helvetica-Condensed', paddingVertical: 30} }>Envie sua sugestão, indique alguma melhoria, reporte algum erro...</H3>
                        <Form>

                            <View style={ {paddingBottom: 20, flex: 1 } }>
                                <Label style={ {...styles.aboutStyle, marginTop: 10} } >O que deseja reportar/sugerir?</Label>
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                                    placeholder="Selecione uma opção... "
                                    placeholderStyle={{ color: "#bfc6ea" }}
                                    placeholderIconColor="#000000"
                                    selectedValue={this.state.kind}
                                    onValueChange={this.onChangeFeedbackKind.bind(this)} >
                                    <Picker.Item label="Erro" value="bug" />
                                    <Picker.Item label="Melhoria" value="enhancement" />
                                    <Picker.Item label="Sugestão" value="proposal" />
                                </Picker>
                            </View>

                            <Textarea rowSpan={5} bordered
                                      style={ styles.aboutStyle }
                                      placeholder="Escreva seu feedback"
                                      value={this.state.content.raw}
                                      onChangeText={ (content) => this.onChangeFeedback(content) }/>

                        </Form>
                    </Content>

                    <Button transparent block dark bordered
                            onPress={ this.sendFeedback.bind(this) } >
                        { this.state.isLoading ? <Spinner color="green"/> : <Text style={ styles.aboutStyle } >Enviar</Text> }
                    </Button>

                </View>

            </Modal>
        );
    }

}

const styles = {

    centerStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
    },
    leftStyle: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    aboutStyle: {
        fontFamily: 'Helvetica-Condensed',
        fontSize: 15,
        paddingVertical: 5,
    }
};

const mapStateToProps = (state) => {
    return { content: state.feedback.content };
}

export default connect( mapStateToProps, {enviarFeedback} )(Feedback);