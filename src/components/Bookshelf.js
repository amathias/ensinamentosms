import _ from 'lodash';
import React from 'react';
import { Content, Container } from 'native-base';
import { withNavigation } from 'react-navigation';

import CardList from './CardList';
import FabSearch from './FabSearch';
import FabRandom from './FabRandom';
import SearchScreen from './SearchScreen';

class Bookshelf extends React.Component {

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;

    title = "Livros de ensinamentos";
    
    if ( params ) {

      if ( params.title ) {
        title = params.title;
      } else if ( params.content.title) {
        title = params.content.title;
      }
      
    }

    return { title }
  };

  render() {

    const { params } = this.props.navigation.state;

    if ( params ) {

      if ( params.content.teachings ) {
        var bookList = params.content.teachings;
      } else {
        var bookList = _.map( params.content , book => {
          return book;
        });

      }
      
    } else {
      var bookList = this.props.books;
    }

    return (
      <Container>

        <Content style={{ backgroundColor: '#63BFA5' }}>
          <CardList list={bookList} />
        </Content>

        <FabSearch books={bookList} />
        <FabRandom books={bookList} />
        {/* <SearchScreen /> */}

      </Container>
        
    );
  }

}

export default withNavigation(Bookshelf);