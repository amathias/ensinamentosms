import React, { Component } from 'react';
import { Text, Icon, Thumbnail, ListItem, Left, Right, Body, Spinner } from 'native-base';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';

import { loadBook } from '../actions';

class CardListItem extends Component {

  onPress() {
    
    let { book } = this.props;

    if ( book.chapter === true ) {
      return;
    }
    
    title = book.title;

    if ( book.key ) {
      book = loadBook( book.key );
    }

    if ( book.content ) {
      this.props.navigation.navigate('Content', { content : book } );
      
    } else {
      this.props.navigation.navigate('Bookshelf', { content : book, title } );
    }
    
  }

  renderIcon(icon) {
    if (!icon) return ;
    return <Left><Icon name={icon} /></Left>;
  }

  renderThumbnail(thumbnail) {
    if (!thumbnail) return ;
    return <Left><Thumbnail square size={55} source={{ uri:thumbnail }} /></Left>;
  }

  renderText(text) {
    if (!text) return;
    return (
        <Text note style={ styles.subtitleStyle }>{text}</Text>
    );
  }

  renderLabel(book) {
    
    if (!book.title) return;

    if ( book.subtitle ) {
      style = styles.titleStyle;
    } else if ( book.chapter === true || book.teachings ) {
      style = styles.chapterStyle;
    } else {
      style = styles.titleStyle;
    }

    note = book.chapter === true;

    return (
        <Text note={ note } 
              style={ style } >
              {book.title}
        </Text>
    );
  }

  renderArrowIcon(book) {

    if ( book.chapter === true ) {
      return;
    }

    return (
      <Right>
          <Icon name="arrow-forward" style={ styles.arrowIconStyle } />
      </Right>
    );
  }

  render() {
    const { book } = this.props;

    return (
      <ListItem 
          button={ book.chapter !== true }
          onPress={ this.onPress.bind(this) } 
          itemDivider={ book.chapter === true }
          style={ book.chapter === true ? { backgroundColor: '#63BFA5' } : {} }
          itemHeader={ book.chapter === true }  >

        {this.renderIcon(book.icon)}
        {this.renderThumbnail(book.thumbnail)}

        <Body>

          { this.renderLabel(book) }
          { this.renderText(book.subtitle) }

        </Body>

        { this.renderArrowIcon(book) }

      </ListItem>

    );

  }

}

const styles = {
  arrowIconStyle: {
    fontFamily: 'Helvetica-Condensed',
    color: 'white', 
    fontSize: 20
  },
  subtitleStyle: {
    fontFamily: 'Helvetica-Condensed',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#699EB0',
    paddingLeft: 10,
  },
  titleStyle: {
    fontFamily: 'Helvetica-Condensed',
    fontSize: 18,
    fontWeight: 'bold',
    paddingTop: 5,
    paddingBottom: 5,
    color: 'white'
  },
  chapterStyle: {
    fontFamily: 'Helvetica-Condensed-Bold',
    fontSize: 16,
    fontWeight: 'bold',
    paddingTop: 10,
    paddingLeft: 10,
    color: '#699EB0'
  }
};

const mapStateToProps = (state) => {
  return { content: state.book.content };
}

export default connect(mapStateToProps, { loadBook } )(withNavigation(CardListItem));