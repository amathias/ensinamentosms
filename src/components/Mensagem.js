import React, { Component } from 'react';
import { H1, Spinner, View, H3, Text } from 'native-base';
import { connect } from 'react-redux';

class Mensagem extends Component {

    render() {
        
        if ( !this.props.content ) {
            return <Spinner style={ this.props.style } color='green' />
        }
        
        const { mensagens } = this.props.content;

        if ( !mensagens || !mensagens.lista ) {
            return (
                <View style = {[ this.props.style, { paddingHorizontal: 25, paddingVertical: 20 }]} >
                    <Text>Em Manutenção</Text>
                </View>
            )
        }
        
        return (
            <View style = {[ this.props.style, { paddingHorizontal: 25, paddingVertical: 20 }]} >
                <H1 style={{ fontFamily: 'Helvetica-Condensed-Bold' }}>{ mensagens.title }</H1>
                <H3 />
                <H3 style={{ fontFamily: 'Helvetica-Condensed' }}>{ mensagens.lista[0].title }</H3>
                <H3/>
                <Text style={{ fontFamily: 'Helvetica-Condensed' }}>{ mensagens.lista[0].content }</Text>
                <Text style={{ fontFamily: 'Helvetica-Condensed' }}>{ mensagens.lista[0].author }</Text>
            </View>
        );
    }

}

const mapStateToProps = (state) => {
    return { content: state.messianica.content };
}
  
export default connect( mapStateToProps, null )(Mensagem);
  