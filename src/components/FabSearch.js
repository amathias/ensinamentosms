import _ from 'lodash';
import React, { Component } from 'react';
import { View,Icon, Fab } from 'native-base';
import { withNavigation } from 'react-navigation';

import { loadBook } from '../actions';

class FabSearch extends Component {
  
  goToSearch() {

    const { params } = this.props.navigation.state;

    if ( params ) {
      
      if ( params.content.teachings ) {
        teachings = params.content.teachings;
      } else {
        
        teachings = _.flatMap( params.content, _book => {
          if ( _book.key ) {
            return _.flatMap( loadBook(_book.key) , book => {
              return book.teachings;
            });
          }

          return _book.teachings;
        });
      }
       
    } else {

      teachings = _.flatMap( this.props.books, _book => {
        return _book.teachings;
      });
    }

    teachings = _.filter( teachings, _book => {
      return _book.chapter !== true;
    });

    this.props.navigation.navigate('Search', { content : teachings } );
  }

  render() {
    return (  
        <View>
          <Fab
            containerStyle={{ }}
            style={{ backgroundColor: '#F2B25A' }}
            position="bottomLeft"
            onPress={ this.goToSearch.bind(this) }>
            <Icon name="search" />
          </Fab>
        </View>
    );
  }
}

export default withNavigation(FabSearch);