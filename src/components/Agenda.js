import React, { Component } from 'react';
import {Text, H1, Spinner, H3, View, H2} from 'native-base';
import { connect } from 'react-redux';

class Agenda extends Component {

    render() {

        if ( !this.props.content ) {
            return (
                <Spinner style={ this.props.style } color='white' />
            );
        }
        
        const { atividades } = this.props.content;

        if ( !atividades || !atividades.agenda ) {
            return (
                <View style={ this.props.style } >
                    <Text>Em Manutenção</Text>
                </View>
            );
        }
        
        return (
            <View style={[ this.props.style, { flex: 1, flexDirection: 'column', paddingBottom: 15 } ]} >
                <View style={{
                        backgroundColor: '#F2B25A', 
                        alignItems: 'center',
                        alignSelf: 'stretch', 
                        padding: 15
                    }} >
                    <H1 style={{ fontFamily: 'Helvetica-Condensed-Bold', color:'#FFF' }}>
                    { atividades.title }
                </H1>
                </View>
                <H2 style={{ fontFamily: 'Helvetica-Condensed', padding: 15, color:'#FFF'  }}>
                    { atividades.agenda[0].desc }
                </H2>
                <H3 style={{ fontFamily: 'Helvetica-Condensed', color: '#F2B25A', paddingBottom: 10 }}>
                    { atividades.agenda[0].date + ' | ' +atividades.agenda[0].hour }
                </H3>
            </View>

        );
    }

}

const mapStateToProps = (state) => {
    return { content: state.messianica };
}
  
export default connect( mapStateToProps, null )(Agenda);
  