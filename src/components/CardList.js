import _ from 'lodash';
import React, { Component } from 'react';
import { FlatList } from "react-native";
import { Content } from 'native-base';

import CardListItem from './CardListItem';

class CardList extends Component {

  renderCardItens(book) {
    return (
      <CardListItem key={ book.index } book={ book.item } />
    );
  }

  extractKey(book, idx) {
    return book.title + '_' + idx;
  }

  render() {

    booklist = _.flatMap(this.props.list, (book, key) => {
      return book;
    });

    return (
      <Content>
        <FlatList
            data={ booklist }
            renderItem={ this.renderCardItens }
            keyExtractor={ this.extractKey } />
      </Content>
    );

  }

}

export default CardList;
