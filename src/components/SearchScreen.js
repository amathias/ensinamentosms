import _ from 'lodash';
import React, { Component } from 'react';
import { Container, Item, Input, Icon, Content } from 'native-base';

import CardList from './CardList';

class SearchScreen extends Component {

    constructor( props ) {
        super( props );

        const { params } = this.props.navigation.state;

        this.state = {
          teachings: [],
          text: '',
          params
        };

    }

    renderSearchBar() {
        return (
            <Item>
                <Icon name="ios-search" style={{ padding: 20, color: 'white' }} />
                <Input placeholder="Pesquisar" 
                    autoFocus = {true}
                    style={{ color: 'white' }}
                    onChangeText={ (text) => this.incrementalSearch( text ) }
                    value={ this.state.text } />
            </Item>
        );
    }

    renderResultList() {
        return (
            <Content>
                <CardList list={ this.state.teachings } />
            </Content>
        );
    }

    incrementalSearch( text ) {
        
        teachings: [];

        if ( text === '' ) {
            this.setState({ text, teachings });
            return;
        }

        teachings = _.filter( this.state.params.content, _tch => {
            return _.includes( JSON.stringify(_tch).toUpperCase(), text.toUpperCase() );
        });

        this.setState({ text , teachings });
    }

    goToContent() {
        this.props.navigation.navigate('Content', { content : this.state.teachings } );
    }

    render() {
        return (
            <Container style={{ backgroundColor: '#63BFA5'}}>
                { this.renderSearchBar() }
                { this.renderResultList() }
            </Container>
        );
    }

}

export default SearchScreen;
