import _ from 'lodash';
import React, { Component } from 'react';
import { View, Icon, Fab } from 'native-base';
import { withNavigation } from 'react-navigation';

import { loadBook } from '../actions';

class FabRandom extends Component {

  randomize() {
    
    const { params } = this.props.navigation.state;

    if ( params ) {
      
      if ( params.content.teachings ) {
        teachings = params.content.teachings;
      } else {
        
        teachings = _.flatMap( params.content, _book => {
          if ( _book.key ) {
            return _.flatMap( loadBook(_book.key) , book => {
              return book.teachings;
            });
          }

          return _book.teachings;
        });
      }
       
    } else {

      teachings = _.flatMap( this.props.books, _book => {
        return _book.teachings;
      });
    }

    teachings = _.filter( teachings, _book => {
      return _book.chapter !== true;
    });

    teaching = _.sample( teachings );

    this.props.navigation.navigate('Content', { content : teaching } );
  }
  
  render() {
    return (  
        <View >
          <Fab
            containerStyle={{ }}
            style={{ backgroundColor: '#F2B25A' }}
            position="bottomRight"
            onPress={ this.randomize.bind(this) }>
            <Icon type="FontAwesome" name="random" />
          </Fab>
        </View>
    );
  }
}

export default withNavigation(FabRandom);