import React, { Component } from 'react';
import {Button, H1, Text, View} from "native-base";
import {Modal, Share} from "react-native";
import Feedback from "./Feedback";

const SHARE_MESSAGE = "Abrir no Expo: exp://exp.host/@mathias85/ensinamentosms.\n\nBaixar Expo:\nAndroid http://bit.ly/2bZq5ew \niOS http://apple.co/2c6HMtp\n";
const SHARE_URL = "https://expo.io/@mathias85/ensinamentosms";

class About extends Component {

    state = {
        modalVisible: false,
        feedbackVisible: false
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    abrirSobreAplicativo() {

        this.setModalVisible(true);
    }

    abrirFeedback() {
        this.setState({feedbackVisible: true});
        this.setModalVisible(false);
    }

    closeFeedback() {
        this.setState({feedbackVisible: false});
        this.setModalVisible(true);
    }

    compartilhar() {
        Share.share({
            message: SHARE_MESSAGE,
            url: SHARE_URL,
            title: 'Compartilhar'
        }, {
            // Android only:
            dialogTitle: 'Compartilhar',
            // iOS only:
            excludedActivityTypes: [
                'com.apple.UIKit.activity.PostToTwitter'
            ]
        });
    }

    render() {
        return(
            <View style={ styles.centerStyle }>

                <Button transparent block dark
                        onPress={ this.abrirSobreAplicativo.bind(this) } >
                    <Text note style={{ fontFamily: 'Helvetica-Condensed-Bold', fontSize: 10 }} >Sobre o aplicativo</Text>
                </Button>

                <Modal
                    animationType="slide"
                    visible={this.state.modalVisible}
                    transparent={true}
                    onRequestClose={ () => this.setModalVisible(false)}>

                    <View style={[styles.centerStyle, { backgroundColor:'#000000AA' } ]}>

                        <View style={ [ { height: 250, marginHorizontal: 30, backgroundColor: '#FFF' } ] }>

                            <H1 style={ {fontFamily: 'Helvetica-Condensed-Bold', padding: 15 } } >Sobre...</H1>

                            <View style={[styles.centerStyle, { paddingHorizontal: 15 }]}>


                                <Text style={ styles.aboutStyle }>
                                    Este aplicativo NÂO foi desenvolvido pela Igreja Messianica Mundial do Brasil.
                                </Text>
                                <Text note style={ {...styles.aboutStyle, fontSize: 12}}>
                                    Desenvoldido por: A&M Apps
                                </Text>

                            </View>

                            <Button transparent block dark
                                    onPress={
                                        this.abrirFeedback.bind(this)
                                    } >
                                <Text style={ styles.aboutStyle } >Enviar Feedback</Text>
                            </Button>

                            <Button transparent block dark
                                    onPress={
                                        this.compartilhar.bind(this)
                                    } >
                                <Text style={ styles.aboutStyle } >Compartilhar</Text>
                            </Button>

                            <Button transparent block dark
                                    onPress={() => {
                                        this.setModalVisible(!this.state.modalVisible);
                                    }} >
                                <Text style={ styles.aboutStyle } >Fechar</Text>
                            </Button>

                        </View>
                    </View>
                </Modal>

                <Feedback feedbackVisible={this.state.feedbackVisible}
                          closeFeedback={this.closeFeedback.bind(this)} />

            </View>

        );
    }

}

const styles = {

    centerStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
    },
    leftStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    aboutStyle: {
        fontFamily: 'Helvetica-Condensed',
        fontSize: 15,
        paddingVertical: 5,
    }

};

export default About;