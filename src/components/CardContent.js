import React from 'react';
import { Text, WebView, View, Platform } from 'react-native';
import { Container, Spinner, Icon } from 'native-base';
import { connect } from 'react-redux';

import { loadMessianicaReadContent } from '../actions';

const styles = `
  <style type="text/css">
      body {
        padding: 25 10;
      }
      h2 {
        text-align: center;
        font-family: 'Helvetica-Condensed', 'Roboto';
        color: #444;
      }
      p {
        text-align: justify;
        font-family: 'Helvetica-Condensed', 'Roboto';
      }
      span {
        float: right;
        font-size: 0.8rem;
        color: #777;
      }
      .c-04 {
        color: #F2B25A;
        float: left;
      }
      .c-06 {
        color: #4B4B4D;
      }
      </style>
  `;

class CardContent extends React.Component {

  constructor(){
    super() ;
  }

  static navigationOptions = ({ navigation, val }) => {
    const { params } = navigation.state;
    
    return {
      headerTitle: <Text style = {{color: '#FFF', fontWeight: 'bold', fontSize: 17}}>
        { !params.content.book ? params.content.title : '' }
      </Text>,
      headerRight: ( 
        <View style={{paddingEnd:10}}>
          {/* <Icon name={ params.favorite ? 'ios-heart' : 'ios-heart-outline' } 
                color="#900" 
                onPress={() => navigation.setParams({favorite: !params.favorite})} /> */}
        </View>),
    }
  };

  componentDidMount() {

    const { params } = this.props.navigation.state;
    const content = params.content;
    
    if ( content.source ) {
      this.props.loadMessianicaReadContent( content.source );
    }

  }

  componentWillUnmount() {
    this.props.loadMessianicaReadContent();
  }

  renderContent( content ) {

    const date = content.info ? `<span class="c-04">Data:</span><span class="c-06"> ${content.info}</span><br>` : undefined;
    let author = content.author ? `<span class="c-04">Por:</span><span class="c-06"> ${content.author}</span><br>` : undefined;
    let editorial = content.book ? `<span class="c-04">Editoria:</span><span class="c-06"> ${content.book}</span><br>` : undefined;

    let html = 
      `<head>${styles}</head>
      <body>
        <h2>${content.title}</h2>
        <p>${content.content}</p>
        </br>
        ${ date || ''}
        ${ author || ''}
        ${ editorial || ''}
        <p>${content.subtitle || ''}</p>
      </body>`;

    return(
      <WebView source = {{ html, baseUrl: 'someUrl'}}
               originWhitelist={ ["*"] }
               scalesPageToFit={ (Platform.OS === 'ios') ? false : true } />
    );
  }

  render() {

    const { params } = this.props.navigation.state;
    let content = params.content;

    if ( content.source ) {
      
      if ( !this.props.content ) {
        return (
          <Container style={{ flex: 1, justifyContent: 'center' }}>
            <Spinner color="#63BFA5"/>
         </Container>
        );
      } else {
        content = this.props.content;
      }

    } 

    return (
      <Container>
        { this.renderContent( content ) }
     </Container>
    );

  }

}

const mapStateToProps = (state) => {
  return { content: state.messianica.readcontent };
}

export default connect(mapStateToProps, { loadMessianicaReadContent } )(CardContent);