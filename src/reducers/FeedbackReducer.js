import { SEND_FEEDBACK } from '../actions';

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case SEND_FEEDBACK:
            return { content: action.payload };
        default:
            return state;
    }

};
