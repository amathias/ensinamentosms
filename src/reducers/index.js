import { combineReducers } from 'redux';
import hardSet from 'redux-persist/lib/stateReconciler/hardSet'
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';

import MessianicaReducer from './MessianicaReducer';
import BookReducer from './BookReducer';
import FeedbackReducer from "./FeedbackReducer";

const persistConfig = {
    key: 'root',
    storage,
    stateReconciler: hardSet,
}

const reducers = combineReducers({
    messianica: MessianicaReducer,
    book: BookReducer,
    feedback: FeedbackReducer,
});

export default persistReducer(persistConfig, reducers);