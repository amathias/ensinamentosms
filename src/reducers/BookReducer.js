import { LOAD_BOOK } from '../actions';

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
      case LOAD_BOOK:
        return { content: action.payload };
      default:
        return state;
    }
  
  };
  