import { 
  LOAD_MESSIANICA_MAIN_CONTENT, 
  LOAD_MESSIANICA_NEWS_CONTENT, 
  LOAD_MESSIANICA_READ_CONTENT 
} from '../actions';

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
      case LOAD_MESSIANICA_MAIN_CONTENT:
        return { ...action.payload };
      case LOAD_MESSIANICA_NEWS_CONTENT:
        return { ...action.payload };
      case LOAD_MESSIANICA_READ_CONTENT:
        return { ...action.payload };
      default:
        return state;
    }
  
  };
  