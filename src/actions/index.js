import _ from 'lodash';
import cio from 'cheerio-without-node-native';
// import cio from 'react-native-cheerio';

export const LOAD_MESSIANICA_MAIN_CONTENT = 'LOAD_MESSIANICA_MAIN_CONTENT';
export const LOAD_MESSIANICA_NEWS_CONTENT = 'LOAD_MESSIANICA_NEWS_CONTENT';
export const LOAD_MESSIANICA_READ_CONTENT = 'LOAD_MESSIANICA_READ_CONTENT';
export const LOAD_BOOK = 'LOAD_BOOK';
export const SEND_FEEDBACK = 'SEND_FEEDBACK';

import alicerceparaiso from '../resources/alicerce-paraiso.json';

const BASE_MAIN_URL = "http://www.messianica.org.br";
const BASE_NOTICIAS_URL = "http://noticias.messianica.org.br";
const NOTICIAS_ENDPOINT = BASE_MAIN_URL; //BASE_NOTICIAS_URL.concat("/home");
const bitBucketIssueTracker = "https://api.bitbucket.org/2.0/repositories/amathias/ensinamentosms/issues";

let $;

const books = {
  alicerceparaiso
}

const Messianica = {
  atividades: [],
  mensagens: []
}

export const loadBook = ( key ) => {
  return books[key];
}

export const enviarFeedback = ( feedback ) => {

    return (dispatch) => {

        if ( !feedback ) {
            dispatch({ type: SEND_FEEDBACK, payload: {created: false} });
            return;
        }

        fetch(bitBucketIssueTracker, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Basic YW1hdGhpYXM6bUB0aDFANThT'
            },
            body: JSON.stringify(feedback),
        })
        .then( res => {
            dispatch({ type: SEND_FEEDBACK, payload: {created: true, id: res.id } });
        })
        .catch((error) => {
            console.error(error);
        });;

    };

}

export const loadMessianicaNewsContent = () => {
  
  return (dispatch) => {

    fetch( NOTICIAS_ENDPOINT )
        .then( res => res.text() )
        .then( res => {
          processNewsResponse(res);
          dispatch({ type: LOAD_MESSIANICA_NEWS_CONTENT, payload: Messianica });
        })
        .catch((error) => {
          console.error(error);
        });
  };

}

export const loadMessianicaMainContent = () => {
  
  return (dispatch) => {

    fetch( BASE_MAIN_URL )
        .then( res => res.text() )
        .then( res => {
          processMainResponse(res);
          dispatch({ type: LOAD_MESSIANICA_MAIN_CONTENT, payload: Messianica });
        })
        .catch((error) => {
          console.error(error);
        });
  };

}

export const loadMessianicaReadContent = ( content ) => {
  return (dispatch) => {

    if ( !content ) {
      Messianica.readcontent = undefined;
      dispatch({ type: LOAD_MESSIANICA_READ_CONTENT, payload: Messianica });
      return;
    }

    fetch( content )
        .then( res => res.text() )
        .then( res => {
          processReadResponse(res);
          dispatch({ type: LOAD_MESSIANICA_READ_CONTENT, payload: Messianica });
        })
        .catch((error) => {
          console.error(error);
        });

  };

}

function processMainResponse( htmlResponse ) {
    
  $ = cio.load( htmlResponse );
  extractEnsinamentoMes();
  extractExperienciaFeMes();
  extractPalestraMes();
}

function processNewsResponse( htmlResponse ) {
    
  $ = cio.load( htmlResponse );
  extractAtividades();
  extractEnsinamentoDia();
  extractExperienciaFeDia();

}

function processReadResponse( htmlResponse ) {

  $ = cio.load( htmlResponse );
  extractReadContent();
}

function extractAtividades() {

  const agenda = $(".agendar").children("div").first();
  
  const atividadeMensal = cio.load( $(agenda).html() );

  const title = _.trim( atividadeMensal("h3.agenda-title").text() );
  let desc = _.trim( atividadeMensal("p.agenda-p").text() );
  desc = _.trim( desc.split(' - ')[0] );
  
  const atividades = [];

  const date = extractDate( atividadeMensal );

  hour = _.trim( atividadeMensal("p.agenda-p").text().split(' - ')[1] );
  // hour = hour.split(' ')[0].length == 1 ? `0${hour}` : hour;
  atividades.push( { title, date, hour, desc } );

  Messianica.atividades = {
      title, desc, agenda: atividades
  }

  // console.log( Messianica.atividades );
}

function extractEnsinamentoDia() {
  
  const agenda = $(".agendar").children("div").eq(1);
  
  const ensinamentoDiario = cio.load( $(agenda).html() );

  const source = ensinamentoDiario("a").attr("href");
  const title = _.trim( ensinamentoDiario("h3.agenda-title").text() );
  const date = extractDate( ensinamentoDiario );

  if ( !Messianica.ensinamento ) {
    Messianica.ensinamento = {};
  }
  Messianica.ensinamento.dia = { 
      title, source, date
  };

  // console.log( Messianica.ensinamento );
}

function extractExperienciaFeDia() {
  
  const agenda = $(".agendar").children("div").eq(2);
  
  const experienciaDiaria = cio.load( $(agenda).html() );

  const source = experienciaDiaria("a").attr("href");
  const title = _.trim( experienciaDiaria("h3.agenda-title").text() );
  const date = extractDate( experienciaDiaria );

  if ( !Messianica.experiencia ) {
    Messianica.experiencia = {};
  }
  Messianica.experiencia.dia = { 
      title, source, date
  };

  // console.log( Messianica.experiencia );

}

function extractEnsinamentoMes() {
  
  const agenda = $(".ul-culto").children("li").first();
  
  const ensinamentoDiario = cio.load( $(agenda).html() );

  const source = ensinamentoDiario("a").attr("href");
  const title = _.trim( ensinamentoDiario("h3.culto-title").text() );
  const date = extractDate( ensinamentoDiario );

  if ( !Messianica.ensinamento ) {
    Messianica.ensinamento = {};
  }
  Messianica.ensinamento.mes = { 
      title, source, date
  };

  // console.log( Messianica.ensinamento );
}

function extractExperienciaFeMes() {
  
  const agenda = $(".ul-culto").children("li").eq(2);
  
  const experienciaDiaria = cio.load( $(agenda).html() );

  const source = experienciaDiaria("a").attr("href");
  const title = _.trim( experienciaDiaria("h3.culto-title").text() );
  const date = extractDate( experienciaDiaria );

  if ( !Messianica.experiencia ) {
    Messianica.experiencia = {};
  }
  Messianica.experiencia.mes = {
      title, source, date
  };

  // console.log( Messianica.experiencia );

}

function extractPalestraMes() {
  
  const agenda = $(".ul-culto").children("li").eq(1);
  
  const experienciaDiaria = cio.load( $(agenda).html() );

  const source = experienciaDiaria("a").attr("href");
  const title = _.trim( experienciaDiaria("h3.culto-title").text() );
  const date = extractDate( experienciaDiaria );

  if ( !Messianica.palestra ) {
    Messianica.palestra = {};
  }
  Messianica.palestra.mes = { 
      title, source, date
  };

  // console.log( Messianica.experiencia );

}

function extractReadContent() {

  const title = $(".section-title-big").html();
  const content = $(".materia-noticias-content").html();
  const subtitle = $(".materia-noticias-subtitle").html()

  Messianica.readcontent = {
    title, content, subtitle
  }

  // console.log( Messianica.readcontent );

}

function extractDate( html ) {

  let date = _.trim( html("p.agenda-data-01").text() );
  date = date + "-" + _.trim( html("p.agenda-data-02").text() );
  date = date.replace(html("b").first().text(), '');
  date = date + "-" + _.trim( html("b").first().text() );

  return date;
}

function removeAfterBreakLine( text ) {
  return _.replace( text, new RegExp("\n[\s]*[\t]*.*", "g"), "");
}