const _ = require('lodash');
var fs = require('fs');

const bookshelf = require('../resources/alicerce-paraiso.json');
const books = require('../resources/books.json');

init();

function init() {

    console.log( "init ... " );

    sources = _.sortBy(books["alicerce-paraiso"].sources, ['title']);

    _.forEach( sources, (source, idx) => {
        console.log( source, idx );
    
        fs.readFile( source.path, function (err, html) {
            if (err) {
                throw err; 
            }
            processResponse(source.title,  html.toString() );
        });

    });

};

function processResponse( title, html ) {

    processContent( title, html );
    writeToJson();
}

function processContent( title, html ) {

    teachingsArray = extractTeachings( html );
    teachings = [];
    
    _.forEach( teachingsArray , parseChapter );

    bookshelf[_.kebabCase(title)] = {
        title,
        teachings
    };

}   

function extractTeachings( html ) {
	
    var teachings = html.split("<br>\n<br>\n<br>\n<br>\n<br>");
	return teachings;
}

function parseChapter( chapter ) {

    array = chapter.split("<br>\n<br>\n<br>\n<br>");
    let _arr = array[1].split("<br>\n<br>\n<br>");

    let partial = _.replace( array[0] ,new RegExp("<br>","g")," - ");
    array = partial.split("\n - ");

    let chapterTitle = clean(_.replace( array[0] ,new RegExp(" - ","g"),""));
    let chapterSubtitle =  array.length > 1 ? clean(array[1]) : undefined;

	var book = {
        chapter : true,
        title: chapterTitle,
        subtitle: chapterSubtitle
    };

    teachings.push( book );

    _.forEach( _arr , (value, key) => {
        parse( value, chapterTitle );    
    });

}

function parse( part, chapter ) {
    
	var split = part.split("<br>\n<br>");
	
	var title = clean( split[0] );
	var content = clean( split[1] );
	var info = clean( split[2] );
	
	if ( !title || !content ) {
        
        console.log("TITLE: ", title);
        console.log("CONTENT", content);
        console.log("INFO", info);
	
	    throw new Error("Erro: Faltanto parte", title, content, info);
	}

	var book = {
        chapter,
        title,
        content,
        info,
        "author": "Meishu-Sama"
    };

    teachings.push( book );
}

function clean( str ) {
    
    str = _.replace( str ,new RegExp("<br><br>","g"),"");
    str = _.replace( str ,new RegExp("<br>","g"),"\n");
    str = _.replace( str ,new RegExp("\n\n","g")," ");
    str = _.trim( str, '\n');

	return str;
}

function writeToJson() {

    let outputPath = books["alicerce-paraiso"].path;

    fs.writeFile(outputPath, JSON.stringify(bookshelf, null, 3), function(error) {
        if (error) {
          console.error("write error:  " + error.message);
        } else {
          console.log("Successful Write to " + outputPath);
        }
   });

}