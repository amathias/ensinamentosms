const fetch = require('node-fetch');
const cheerio = require('cheerio-without-node-native');
const _ = require('lodash');
var fs = require('fs');

const endpoint = "http://www.messianica.org.br";

init();

var $;

var Messianica = {
    atividades: [],
    mensagens: [],
    ensinamento: {},
    experiencia: {}
}

function init() {

    console.log( "init ... " );

    fetch( endpoint )
        .then( res => res.text() )
        .then( processResponse );
};

function processResponse( htmlResponse ) {
    
    $ = cheerio.load( htmlResponse );
    extractAtividades();
    extractMensagens();
    extractEnsinamentoDia();
    extractExperienciaFeDia();
}

function extractAtividades() {

    title = _.trim( $(".ativ-title").text() );
    title = removeAfterBreakLinke( title );

    desc = _.trim( $(".ativ-desc").text() );
    desc = removeAfterBreakLinke( desc);

    let agenda = [];

    let tabs = $("#tabs-atividades").children("div");
    $(tabs).each( (i, elem ) => {

        const ativ = cheerio.load( $(elem).html() );

        title = _.trim( ativ(".ativ-title").text() );

        date = _.trim( ativ(".ativ-date").text() );
        date = _.replace( _.replace( date, new RegExp("[\s]*\n[\s]*[\t]*", "g"), "/"), "//", "/");

        hour = _.trim( ativ(".ativ-hour").text() );
        hour = _.trim( removeAfterBreakLinke( hour ) );
        
        desc = _.trim( ativ(".ativ-desc").text() );
        desc = _.trim( removeAfterBreakLinke( desc ) );
        
        agenda.push( { title, date, hour, desc } );
    
    });

    Messianica.atividades = {
        title, desc, agenda
    }

    // console.log( Messianica.atividades );
}

function extractMensagens() {


    let title = $(".slide-text-title").text();

    let msgs = $("#slide-text > ul").children("li");
    let lista = [];

    $(msgs).each( (i, elem ) => {

        const msg = cheerio.load( $(elem).html() );

        msgTitle = msg("p").html();

        author = _.trim( msg("*").text() ).match("[(].*[)]$")[0];

        content = _.trim( _.replace( msg.text(), msgTitle, "") );
        content = _.trim( _.replace( content, author, "") );

        lista.push( { title: msgTitle, content, author } )
    });


    Messianica.mensagens = {
        title, lista
    };

    // console.log( Messianica.mensagens );
}

function extractEnsinamentoDia() {

    const loopBox = $(".dest-loop-box")[2];
    
    const ensinamentoDiario = cheerio.load( $(loopBox).children("div").children("div").first().html() );

    source = ensinamentoDiario("a").attr("href");
    image = ensinamentoDiario("img").attr("src");
    title = _.trim( ensinamentoDiario("span").text() );

    Messianica.ensinamento = { 
        title, source, image 
    };

    // console.log( Messianica.ensinamento );
}

function extractExperienciaFeDia() {

    const loopBox = $(".dest-loop-box")[2];

    const experienciaDiaria = cheerio.load( $(loopBox).children("div").children("div").eq(1).html() );

    source = experienciaDiaria("a").attr("href");
    image = experienciaDiaria("img").attr("src");
    title = _.trim( experienciaDiaria("span").text() );

    Messianica.experiencia = { 
        title, source, image 
    };

    // console.log( Messianica.experiencia );

}

function removeAfterBreakLinke( text ) {
    return _.replace( text, new RegExp("\n[\s]*[\t]*.*", "g"), "");
}